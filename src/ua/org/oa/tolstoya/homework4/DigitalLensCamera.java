package ua.org.oa.tolstoya.homework4;

/**
 * Created by Alex on 08.10.2016.
 */
public class DigitalLensCamera extends PhotoEquipment {
    private String changeOptics;

    public DigitalLensCamera(String brand, String model, double resolution, double price, String changeOptics) {
        super(brand, model, resolution, price);
        setChangeOptics(changeOptics);
    }

    public String getChangeOptics() {
        return changeOptics;
    }

    public void setChangeOptics(String changeOptics) {
        this.changeOptics = changeOptics;
    }
}
