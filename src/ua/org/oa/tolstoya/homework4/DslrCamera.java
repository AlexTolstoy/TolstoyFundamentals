package ua.org.oa.tolstoya.homework4;

/**
 * Created by Alex on 08.10.2016.
 */
public class DslrCamera extends PhotoEquipment {
    private String imageSensor;
    private String changeOptics;

    public DslrCamera(String brand, String model, double resolution, double price, String imageSensor, String changeOptics) {
        super(brand, model, resolution, price);
        setImageSensor(imageSensor);
        setChangeOptics(changeOptics);
    }

    public String getImageSensor() {
        return imageSensor;
    }

    public void setImageSensor(String imageSensor) {
        this.imageSensor = imageSensor;
    }

    public String getChangeOptics() {
        return changeOptics;
    }

    public void setChangeOptics(String changeOptics) {
        this.changeOptics = changeOptics;
    }
}
