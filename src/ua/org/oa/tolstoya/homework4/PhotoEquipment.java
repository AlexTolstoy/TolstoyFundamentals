package ua.org.oa.tolstoya.homework4;

/**
 * Created by Alex on 08.10.2016.
 */
public class PhotoEquipment {
    private String brand;
    private String model;
    private double resolution;
    private double price;

    public PhotoEquipment(String brand, String model, double resolution, double price) {
        setBrand(brand);
        setModel(model);
        setResolution(resolution);
        setPrice(price);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getResolution() {
        return resolution;
    }

    public void setResolution(double resolution) {
        this.resolution = resolution;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", resolution=" + resolution +
                ", price=" + price +
                '}';
    }
}
