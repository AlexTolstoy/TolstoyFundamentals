package ua.org.oa.tolstoya.homework4;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Alex on 09.10.2016.
 */
public class App {
    public static void main(String[] args) {
        CameraList cameraList = new CameraList();
        cameraList.addPhotoEquipment(new DigitalCamera("Sony", "RX100", 20.2, 100));
        cameraList.addPhotoEquipment(new DigitalCamera("Canon", "PowerShot N", 12.1, 80));
        cameraList.addPhotoEquipment(new DigitalCamera("Panasonic", "Lumix ZS20", 14.1, 120));
        cameraList.addPhotoEquipment(new DigitalLensCamera("Olympus", "PEN-F", 20.3, 1200, "Lens"));
        cameraList.addPhotoEquipment(new DigitalLensCamera("Fujifilm", "X-100", 12, 800, "Lens"));
        cameraList.addPhotoEquipment(new DigitalLensCamera("Sony", "a7 II", 24.3, 1400, "Lens"));
        cameraList.addPhotoEquipment(new DslrCamera("Canon", "EOS 5D Mark IV", 30.4, 5000, "Full Frame", "Lens"));
        cameraList.addPhotoEquipment(new DslrCamera("Nikon", "D5100", 16.9, 400, "APS-C", "Lens"));
        cameraList.addPhotoEquipment(new DslrCamera("Canon", "EOS 1200D", 18.7, 400, "APS-C", "Lens"));
        cameraList.addPhotoEquipment(new DslrCamera("Sony", "Alpha", 43.6, 3000, "Full Frame", "Lens"));

        cameraList.cameraResolution(18);
        System.out.println("Полнокадровые фотоаппараты: ");
        cameraList.imageSensor();
        System.out.println("Камеры со сменной оптикой, в заданом ценовом диапазоне: ");
        cameraList.lensCamers(500, 2000);
    }
}
