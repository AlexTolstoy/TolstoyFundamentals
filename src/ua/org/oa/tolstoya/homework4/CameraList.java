package ua.org.oa.tolstoya.homework4;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 09.10.2016.
 */
public class CameraList {
    private List<PhotoEquipment> cameras = new ArrayList<>();

    void addPhotoEquipment(PhotoEquipment camera) {
        cameras.add(camera);
    }

    PhotoEquipment takePhotoEquipment(String brand, String model) {
        for (PhotoEquipment camera : cameras) {
            if (brand.equals(camera.getBrand()) && model.equals(camera.getModel())) {
                return camera;
            }
        }
        return null;
    }

    void cameraResolution(double resolution) {
        for (PhotoEquipment camera : cameras) {
            if (camera.getResolution() > resolution) {
                System.out.println(camera);
            }
        }
    }

    void imageSensor() {
        for (PhotoEquipment camera : cameras) {
            if (camera instanceof DslrCamera) {
                if (((DslrCamera) camera).getImageSensor().equals("Full Frame")) {
                    System.out.println(camera);
                }
            }
        }
    }

    void lensCamers(double a, double b) {
        for (PhotoEquipment camera : cameras) {
            if (camera instanceof DigitalLensCamera) {
                if (camera.getPrice() > a && camera.getPrice() < b) {
                    System.out.println(camera);
                }
            }
        }

    }
}