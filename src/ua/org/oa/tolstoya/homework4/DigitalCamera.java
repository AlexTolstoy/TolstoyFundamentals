package ua.org.oa.tolstoya.homework4;

/**
 * Created by Alex on 08.10.2016.
 */
public class DigitalCamera extends PhotoEquipment {
    public DigitalCamera(String brand, String model, double resolution, double price) {
        super(brand, model, resolution, price);
    }
}
