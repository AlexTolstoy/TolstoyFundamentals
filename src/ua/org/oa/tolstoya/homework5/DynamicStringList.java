package ua.org.oa.tolstoya.homework5;

import java.util.Arrays;

/**
 * Created by Alex on 11.10.2016.
 */
public class DynamicStringList implements SimpleList {
    private String[] strings;
    private int size = 0;

    public DynamicStringList() {
        strings = new String[0];
    }

    public DynamicStringList(int size) {
        strings = new String[size];
    }

    @Override
    public void add(String s) {
        if(size >= strings.length) {
            newArray();
        }
        strings[size++] = s;
    }

    @Override
    public String get() {
        return strings[size - 1];
    }

    @Override
    public String get(int id) {
        return strings[id];
    }

    @Override
    public String remove() {
        String z = strings[size - 1];
        strings[size - 1] = null;
        size--;
        return z;
    }

    @Override
    public String remove(int id) {
        String z = strings[id];
        System.arraycopy(strings, id + 1, strings, id, (size - 1) - id);
        strings[size - 1] = null;
        size--;
        return z;
    }

    @Override
    public boolean delete() {
        for (int i = 0; i < size ; i++) {
            strings[i] = null;
        }
        size = 0;
        return true;
    }

    void newArray() {
        String[] temp = new String[size * 2];
        System.arraycopy(strings, 0, temp, 0, strings.length);
        strings = temp;
    }

    @Override
    public String toString() {
        return "DynamicStringList:" + "\n" +
                Arrays.toString(strings) + "\n" +
                "Size = " + size;
    }

}
