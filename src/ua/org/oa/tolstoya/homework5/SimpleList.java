package ua.org.oa.tolstoya.homework5;

/**
 * Created by Alex on 11.10.2016.
 */
public interface SimpleList {
    void add(String s);
    String get();
    String get(int id);
    String remove();
    String remove(int id);
    boolean delete();
}
