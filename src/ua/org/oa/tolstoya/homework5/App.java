package ua.org.oa.tolstoya.homework5;

/**
 * Created by Alex on 20.10.2016.
 */
public class App {
    public static void main(String[] args) {
        DynamicStringList list = new DynamicStringList(2);
        list.add("One");
        list.add("Two");
        list.add("Three");
        list.add("Four");
        list.add("Five");
        System.out.println(list);
        System.out.println(list.get(2));
        System.out.println(list.get());
        System.out.println(list.remove());
        System.out.println(list.remove(1));
        System.out.println(list);
        System.out.println(list.delete());
        System.out.println(list);
    }
}
