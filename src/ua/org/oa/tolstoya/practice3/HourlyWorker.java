package ua.org.oa.tolstoya.practice3;

/**
 * Created by Alex on 03.10.2016.
 */
public class HourlyWorker extends Employee {
    private final int HOURSNORMA = 140;
    private final double COEFFICIENT = 1.5;
    private double stavka;
    private int workHours;

    public HourlyWorker(String name, String surname, double stavka, int workHours) {
        super(name, surname);
        setStavka(stavka);
        setWorkHours(workHours);
    }

    @Override
    public double payroll() {
        double salary = 0;
        if (workHours > 0) {
            if (workHours <= HOURSNORMA) {
                salary = stavka * workHours;
            } else {
                salary = (stavka * HOURSNORMA) + ((workHours - HOURSNORMA) * stavka * COEFFICIENT);
            }
        } else {
            System.out.println("Введите число больше 0");
        }
        return salary;
    }

    public double getStavka() {
        return stavka;
    }

    public void setStavka(double stavka) {
        this.stavka = stavka;
    }

    public int getWorkHours() {
        return workHours;
    }

    public void setWorkHours(int workHours) {
        if(workHours > 0) {
            this.workHours = workHours;
        } else  {
            System.out.println("Input...");
        }
    }
}
