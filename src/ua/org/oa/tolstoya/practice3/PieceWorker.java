package ua.org.oa.tolstoya.practice3;

/**
 * Created by Alex on 03.10.2016.
 */
public class PieceWorker extends Employee {
    private double sdelka;
    private int amountProduct;


    public PieceWorker(String name, String surname, double sdelka, int amountProduct) {
        super(name, surname);
        setSdelka(sdelka);
        setAmountProduct(amountProduct);
    }

    public double getSdelka() {
        return sdelka;
    }

    public void setSdelka(double sdelka) {
        this.sdelka = sdelka;
    }

    public int getAmountProduct() {
        return amountProduct;
    }

    public void setAmountProduct(int amountProduct) {
        this.amountProduct = amountProduct;
    }

    @Override
    public double payroll() {
        return sdelka * amountProduct;
    }
}
