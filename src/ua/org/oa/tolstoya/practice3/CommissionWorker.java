package ua.org.oa.tolstoya.practice3;

/**
 * Created by Alex on 03.10.2016.
 */
public class CommissionWorker extends Employee {
    private final int STAVKA = 6000;
    private final int PERCENT = 5;
    private int sales;

    public CommissionWorker(String name, String surname) {
        super(name, surname);
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    @Override
    public double payroll() {
        return ((sales / 100) * PERCENT) + STAVKA;
    }
}
