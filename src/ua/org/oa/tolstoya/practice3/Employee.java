package ua.org.oa.tolstoya.practice3;

/**
 * Created by Alex on 03.10.2016.
 */
public abstract class Employee {
    private String name;
    private String surname;

    public Employee(String name, String surname) {
        setName(name);
        setSurname(surname);
    }

    public abstract double payroll();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", salary='" + payroll() + '\'' +
                '}';
    }
}
