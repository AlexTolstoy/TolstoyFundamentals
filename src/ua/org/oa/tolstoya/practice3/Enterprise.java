package ua.org.oa.tolstoya.practice3;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 03.10.2016.
 */
public class Enterprise {
    private String enterpriseName;
    private List<Employee> workers = new ArrayList<>();

    public Enterprise(String enterpriseName) {
        setEnterpriseName(enterpriseName);
    }

    void addEmployee(Employee worker) {
        workers.add(worker);
    }

    Employee takeEmployee(String name) {
        for (Employee worker : workers) {
            if (name.equals(worker.getName())) {
                return worker;
            }
        }
        return null;
    }

    boolean deleteEmployee (String name) {
        Employee worker = takeEmployee(name);
        if (worker != null) {
            return workers.remove(worker);
        }
        return false;
    }

    void salaryEmployee () {
        double salary = 0;
        for (Employee worker : workers) {
            System.out.println(worker);
            salary += worker.payroll();
        }
        System.out.println("Salary All Employee = " + salary);
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }
}
