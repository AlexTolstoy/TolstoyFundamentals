package ua.org.oa.tolstoya.practice3;

/**
 * Created by Alex on 03.10.2016.
 */
public class Boss extends Employee {
    private double salary;

    public Boss(String name, String surname, int salary) {
        super(name, surname);
        setSalary(salary);
    }

    @Override
    public double payroll(){
        return salary * 4;

    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
