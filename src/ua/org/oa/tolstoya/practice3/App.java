package ua.org.oa.tolstoya.practice3;

/**
 * Created by Alex on 03.10.2016.
 */
public class App {
    public static void main(String[] args) {
        Employee boss = new Boss("Vasay", "Ivanovich", 3500);
        Employee hourlyWorker = new HourlyWorker("Petya", "Pupkin", 50, 8);
        System.out.println(boss);
        System.out.println("Зарплата Boss = " + boss.payroll());
        System.out.println(hourlyWorker);
        System.out.println("Зарплата HourlyWorker = " + hourlyWorker.payroll());

        Enterprise enterprise = new Enterprise("Konstanta");
        enterprise.addEmployee(boss);
        enterprise.addEmployee(hourlyWorker);
        enterprise.addEmployee(new PieceWorker("Vas", "Mas", 25, 50));
        enterprise.salaryEmployee();
        enterprise.deleteEmployee("Vasay");
        enterprise.salaryEmployee();
    }
}
