package ua.org.oa.tolstoya.homework6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.Inflater;

/**
 * Created by Alex on 24.10.2016.
 */
public class ExeptionUtils {

    static void arrayIndexOutOf() {
        int i = 4;
        int[] array = new int[3];
        try {
            array[i] = i;
            i++;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(e);
        }
        System.out.println(Arrays.toString(array));
    }

    static void illegalArgumentException(int i) {
        try {
            ArrayList<String> strings = new ArrayList<>(i);
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

    }
    static void classCast() {
        try {
            Object object = new String();
            Integer z = (Integer) object;
        } catch (ClassCastException e) {
            System.out.println(e);
        }

    }

    static void stringIndexOut() {
        try {
            String s = "Hello";
            s.charAt(7);
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println(e);
        }
    }

    static void nullPointer() {
        try {
            String[] array = new String[5];
            array[3].length();
        } catch (NullPointerException e) {
            System.out.println(e);
        }
    }

    static void stackOverflow() {
        try {
            stackOverflow();
        } catch (StackOverflowError e){
            System.out.println(e);
        }
    }

    static void numberFormat() {
        try {
            String s = "130";
            byte b = Byte.parseByte(s);
            System.out.println(b);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }

    static void outOfMemory() {
        try {
            StringBuilder sb = new StringBuilder("New");
            for (int i = 0; i < sb.length(); i++) {
                sb.insert(i + 1, sb.charAt(i));
            }
        } catch (OutOfMemoryError e){
            System.out.println(e);
        }
    }
}
