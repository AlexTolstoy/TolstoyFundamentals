package ua.org.oa.tolstoya.homework6;

/**
 * Created by Alex on 24.10.2016.
 */
public class ExeptionUtilsTest {
    public static void main(String[] args) {
        ExeptionUtils.arrayIndexOutOf();
        ExeptionUtils.illegalArgumentException(-1);
        ExeptionUtils.classCast();
        ExeptionUtils.stringIndexOut();
        ExeptionUtils.nullPointer();
        ExeptionUtils.stackOverflow();
        ExeptionUtils.numberFormat();
        ExeptionUtils.outOfMemory();
    }
}
