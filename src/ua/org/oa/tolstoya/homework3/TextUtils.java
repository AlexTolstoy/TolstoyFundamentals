package ua.org.oa.tolstoya.homework3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Alex on 30.09.2016.
 */
public class TextUtils {
    static int calcuteNumbers(String file) throws IOException {
        String text = textOfFile(file);
        String numbers = "0123456789";
        int sum = 0;
        int num = 0;
        int counter = 0;
        int test = 0;
        for (int i = text.length() - 1; i >= 0; i--) {
            if((num = numbers.indexOf(text.charAt(i))) != -1){
                test = num * (int)Math.pow(10, counter++);
                System.out.println(test);
                sum += test;
            }else{
                counter = 0;
            }

        }
        return sum;
    }

    static String textOfFile (String file) throws IOException {
        BufferedReader bf = new BufferedReader(new FileReader(file));
        StringBuilder contents = new StringBuilder();
        String text = null;
        while ((text = bf.readLine()) != null) {
            contents.append(text).append("\n");
        }
        return contents.toString();
    }
}
