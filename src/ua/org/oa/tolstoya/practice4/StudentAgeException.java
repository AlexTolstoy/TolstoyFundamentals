package ua.org.oa.tolstoya.practice4;

/**
 * Created by Alex on 10.10.2016.
 */
public class StudentAgeException extends StudentException {
    public StudentAgeException(String message) {
        super(message);
    }
}
