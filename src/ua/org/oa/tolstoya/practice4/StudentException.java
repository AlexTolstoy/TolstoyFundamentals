package ua.org.oa.tolstoya.practice4;

/**
 * Created by Alex on 10.10.2016.
 */
public class StudentException extends Exception {
    public StudentException(String message) {
        super(message);
    }
}
