package ua.org.oa.tolstoya.practice4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Created by Alex on 10.10.2016.
 */
public class Group {
    private String name;
    private Student[] students;

    public Group(String name, int amountSudent) {
        setName(name);
        students = new Student[amountSudent];
    }

    boolean addStudent (int index) {
        System.out.println("Input " + (index+1) + "-th student's data:");
        Student std = createStudentConsole();
        if(studentValidate(std)) {
            students[index] = std;
            return true;
        }
        return  false;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String readString(BufferedReader br, String s) throws IOException {
        System.out.println(s);
        return br.readLine();
    }

    int readNumber(BufferedReader br, String s) throws IOException {
        int number;
        while (true) {
            try {
                System.out.println(s);
                number = Integer.parseInt(br.readLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Wrong number try again");
            }
        }
        return number;
    }

    Student createStudentConsole() {
        Student student = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new UncloseInputSream(System.in)))) {
            String name = readString(br, "Input student's name:");
            int age = readNumber(br, "Input student's age:");
            while (true) {
                try {
                    student = new Student(name, age);
                    break;
                } catch (StudentAgeException e) {
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                    age = readNumber(br, "Input age again:");
                } catch (StudentNameException e) {
                    System.out.println(e.getMessage());
                    name = readString(br, "Input name again:");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return student;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", students=" + Arrays.toString(students) +
                '}';
    }

    void feelGroup() {
        for (int i = 0; i < students.length;) {
           if(addStudent(i)){
               i++;
           }
        }
        System.out.println(getName() + ": ");
        int index = 1;
        for (Student student : students) {
            System.out.println(index++ + " " + student);
        }

    }

    boolean studentValidate(Student std) {
        for (Student student : students) {
            if(std.equals(student)) {
                System.out.println("Already exists: " + std);
              return false;
            }
        }
        return true;
    }
}
