package ua.org.oa.tolstoya.practice4;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Alex on 10.10.2016.
 */
public class UncloseInputSream extends FilterInputStream {
    protected UncloseInputSream(InputStream in) {
        super(in);
    }

    @Override
    public void close() throws IOException {
        //NOP
    }
}
