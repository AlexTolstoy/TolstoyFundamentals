package ua.org.oa.tolstoya.practice4;

/**
 * Created by Alex on 10.10.2016.
 */
public class StudentNameException extends StudentException {
    public StudentNameException(String message) {
        super(message);
    }
}
