package ua.org.oa.tolstoya.practice4;

/**
 * Created by Alex on 10.10.2016.
 */
public class Student {
    private static final int MIN_AGE = 17;
    private static final int MAX_AGE = 45;
    private static final int MIN_NAME_LENGTH = 3;
    private String name;
    private int age;

    public Student(String name, int age) throws StudentNameException, StudentAgeException {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws StudentNameException {
        if(name.length() >= MIN_NAME_LENGTH) {
            this.name = name;
        } else {
            throw new StudentNameException("Wrong name, use name length more then " + MIN_NAME_LENGTH);
        }

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws StudentAgeException {
        if(age >= MIN_AGE && age <= MAX_AGE){
            this.age = age;
        }else {
            throw new StudentAgeException("Wrong age, use age from " + MIN_AGE + " to " + MAX_AGE);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (age != student.age) return false;
        return name != null ? name.equals(student.name) : student.name == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
