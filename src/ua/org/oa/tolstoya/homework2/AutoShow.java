package ua.org.oa.tolstoya.homework2;

import java.util.Arrays;

/**
 * Created by Alex on 21.09.2016.
 */
class AutoShow {
    private String name;
    private Car[] cars;

    AutoShow(String name, int amountCar) {
        this.name = name;
        cars = new Car[amountCar];
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    void addCar(Car car, int keyIndex) {
        cars[keyIndex] = car;
    }

    Car takeCar(int keyIndex) {
        return cars[keyIndex];
    }

    double mileage() {
        double sumMileage = 0;
        int count = 0;
        for (Car car : cars) {
            if (car != null) {
                sumMileage += car.getMileage();
                count++;
            }
        }
        return sumMileage / count;
    }

    void mileage (double mileage){
        for (Car car : cars) {
            if (car != null && car.getMileage() > mileage) {
                System.out.println(car);
            }
        }
    }

    void lowmileage (double mileage){
        for (Car car : cars) {
            if (car != null && car.getMileage() < mileage) {
                System.out.println(car);
            }
        }
    }

    void lowaveragemileage (){
        for (Car car : cars) {
            if (car != null && car.getMileage() < mileage()) {
                System.out.println(car);
            }
        }
    }
    void highaveragemileage (){
        for (Car car : cars) {
            if (car != null && car.getMileage() > mileage()) {
                System.out.println(car);
            }
        }
    }
}
