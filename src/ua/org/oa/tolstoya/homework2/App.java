package ua.org.oa.tolstoya.homework2;

/**
 * Created by Alex on 21.09.2016.
 */
public class App {
    public static void main(String[] args) {
        AutoShow autoShow = new AutoShow("Автосалон подержаных автомобилей", 14);
        autoShow.addCar(new Car("Audi A5 3.2 (8T)", 57_207, 2011), 1);
        autoShow.addCar(new Car("Audi A6 3.2 FSI (C6)", 89_351, 2011), 2);
        autoShow.addCar(new Car("Audi Q7 4.2 TDI (4L)", 42_355, 2015), 3);
        autoShow.addCar(new Car("BMW 540i (E39)", 163_765, 2003), 4);
        autoShow.addCar(new Car("BMW 540i (E60)", 87_533, 2010), 5);
        autoShow.addCar(new Car("BMW X5 4.4i (E70)", 75_350, 2013), 6);
        autoShow.addCar(new Car("Mercedes-Benz E320 (W211)", 193_101, 2009), 7);
        autoShow.addCar(new Car("Mercedes-Benz CLS 500 (W211)", 68_783, 2013), 8);
        autoShow.addCar(new Car("Mercedes-Benz GL 500 (X164)", 113_203, 2012), 9);
        autoShow.addCar(new Car("Porsche Cayenne Turbo 4.8 (957)", 132_801, 2010), 10);
        autoShow.addCar(new Car("Infiniti FX35 (S51)", 97_035, 2012), 11);
        autoShow.addCar(new Car("Lada 21099", 200_000), 12);


        System.out.println("Средний пробег подержаных автомобилей = "  + autoShow.mileage());
        System.out.println("~~~~~~~~~~~ Машины с пробегом выше заданого ~~~~~~~~~~~");
        autoShow.mileage(115_000);
        System.out.println("~~~~~~~~~~~ Машины с пробегом ниже заданого ~~~~~~~~~~~");
        autoShow.lowmileage(50_000);
        System.out.println("~~~~~~~~~~~ Машины с пробегом ниже среднего ~~~~~~~~~~~");
        autoShow.lowaveragemileage();
        System.out.println("~~~~~~~~~~~ Машины с пробегом выше среднего ~~~~~~~~~~~");
        autoShow.highaveragemileage();
    }
}
