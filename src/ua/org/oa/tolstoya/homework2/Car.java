package ua.org.oa.tolstoya.homework2;

/**
 * Created by Alex on 21.09.2016.
 */
class Car {
    private String brandModel;
    private double mileage;
    private int year;

    Car(){
    }

    Car(String brandModel, double mileage, int year) {
        this.brandModel = brandModel;
        this.mileage = mileage;
        this.year = year;
    }

    Car(String brandModel, double mileage) {
        this.brandModel = brandModel;
        this.mileage = mileage;
    }

    String getBrandModel() {
        return brandModel;
    }

    void setBrandModel(String brandModel) {
        this.brandModel = brandModel;
    }

    double getMileage() {
        return mileage;
    }

    void setMileage(double mileage) {
        this.mileage = mileage;
    }

    int getYear() {
        return year;
    }

    void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Car{" +
                "brandModel='" + brandModel + '\'' +
                ", mileage=" + mileage +
                ", year=" + year +
                '}';
    }
}
