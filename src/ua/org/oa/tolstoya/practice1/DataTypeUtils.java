package ua.org.oa.tolstoya.practice1;

/**
 * Created by Alex on 11.09.2016.
 */
public class DataTypeUtils {
    public static byte a;
    public static short b;
    public static int c;
    public static long d;
    public static float e;
    public static double f;
    public static char gchar;
    public static boolean hboolean;
    public static short sumshort;

    public static int intNumbersSum(int start, int end) {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            sum += i;
        }
        return sum;
    }

    public static void primitivType() {
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(gchar);
        System.out.println(hboolean);


        byte a = 2;
        short b = 200;
        int c = 1000;
        long d = 999999;
        float e = 1.5f;
        double f = 3.2;
        char gchar = 'C';
        boolean hboolean = true;
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        System.out.println(gchar);
        System.out.println(hboolean);

    }

    public static void typesFloat() {
        float re = 1.f;
        float te = 1;
        float ve = 0x1;
        float ii = 0b1;
        float ty = 1.0e1f;
        float hy = 01;
        System.out.println(re);
        System.out.println(te);
        System.out.println(ve);
        System.out.println(ii);
        System.out.println(ty);
        System.out.println(hy);
    }

    public static void task3() {
        sumshort = 2 + 3;
        System.out.println("Сумма двух целых чисел = " + sumshort);
        sumshort = (short) 1.2 + 1;
        System.out.println("Сумма дробного и целого числа = " + sumshort);
        int mm = 30;
        float nn = 0.9f;
        sumshort = (short) (mm + nn);
        System.out.println("Сумма int и float = " + sumshort);
        byte bb = 15;
        short cc = 300;
        sumshort = (short) (bb + cc);
        System.out.println("Сумма byte и short = " + sumshort);
        float ll = -12;
        double kk = 4.3;
        sumshort = (short) (ll + kk);
        System.out.println("Сумма float и double = " + sumshort);

    }
    public static void task4(){
        double a = Math.pow(6, 2);
        double b = Math.pow(8, 2);
        double c = Math.pow(10, 2);
        System.out.println(c == a + b ? "Прямой треугольник" : "Непрямой треугольник");
    }

    public static int task6(int start6, int end6) {
        int sum = 0;
        for (int i = start6; i <= end6; i++) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        return sum;
    }

}
