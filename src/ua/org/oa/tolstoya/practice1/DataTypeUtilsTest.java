package ua.org.oa.tolstoya.practice1;

/**
 * Created by Alex on 11.09.2016.
 */
public class DataTypeUtilsTest {
    public static void main(String[] args) {
        System.out.println("Test for 5 task");
        int start = 1;
        int end = 20;
        int result = DataTypeUtils.intNumbersSum(start, end);
        System.out.println(" Sum of numbers " + start + " to " + end + " is " + result);
        DataTypeUtils.primitivType();
        DataTypeUtils.typesFloat();
        DataTypeUtils.task3();
        DataTypeUtils.task4();
        int start6 = 1;
        int end6 = 20;
        int result6 = DataTypeUtils.task6(start6, end6);
        System.out.println("Сумма четных чисел от " + start6 + " до " + end6 + " = " + result6);

    }
}
