package ua.org.oa.tolstoya.practice1;

/**
 * Created by Alex on 18.09.2016.
 */
public class Task10 {
    public static double credit = 12000;
    public static double stavka = 12;
    public static int month = 12;

    public static void main(String[] args) {
        final double MONTH_PART = credit / month;
        double percent;
        double percent_sum = 0;
        double debt_sum = 0;
        double sum = 0;
        for (int i = 1; i <= month; i++) {
            percent = credit * stavka / 100 / 12;
            percent_sum += percent;
            debt_sum += MONTH_PART;
            sum = MONTH_PART + percent;
            System.out.print(i + "  " + credit + "  " + percent + "  " + MONTH_PART + "  " + sum);
            System.out.println();
            credit = credit - MONTH_PART;
        }
        System.out.println("Выплачено процентов = " + percent_sum);
        System.out.println("Выплачено по кредиту = " + debt_sum);
        System.out.println("ИТОГО = " + sum);
    }
}
