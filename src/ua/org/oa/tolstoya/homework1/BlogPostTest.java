package ua.org.oa.tolstoya.homework1;

/**
 * Created by Alex on 20.09.2016.
 */
public class BlogPostTest {
    public static void main(String[] args) {

        // Объявление, выделение памяти и инициализация обектов типа BlogPost
        BlogPost myPost1 = new BlogPost(false, "Доллар взлетит до 30 грн", "Internet", 50, 0);
        BlogPost myPost2 = new BlogPost(true, "The New 2016 Audi A5 Coupe", "Audi.com", 300, 298);
        BlogPost myPost3 = new BlogPost(true, "Как стать программистом", "Алексей Толстой", 10, 0);

        // Проверка публикации первого поста
        System.out.println("~~~~~~~~~~~~Post 1~~~~~~~~~~~~");
        myPost1.publish();
        System.out.println();

        // Проверка публикации второго поста
        System.out.println("~~~~~~~~~~~~Post 2~~~~~~~~~~~~");
        myPost2.publish();
        System.out.println();

        // Проверка публикации третьего поста
        System.out.println("~~~~~~~~~~~~Post 3~~~~~~~~~~~~");
        myPost3.publish();
    }
}
