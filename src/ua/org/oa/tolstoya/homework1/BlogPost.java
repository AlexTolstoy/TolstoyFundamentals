package ua.org.oa.tolstoya.homework1;

/**
 * Создать java-программу, удовлетворяющую следующим требованиям:
 * 1. Создать класс, описывающий заданный объект. В каждом классе:
 *      * Определить минимум 5 полей трех различных типов характеризующих заданный предмет;
 *      * Создать метод, выполняющий вычисление параметров на основании значений полей объекта;
 *      * Определить метод для вывода информации об объекте на экран.
 * 2. Каждый класс, поле, метод должен иметь название, отражающее его суть и коментарии, описывающие их предназначение.
 * 3. На этапе составления программы должны быть использованы соглашения из java code convention.
 * 4. Протестировать созданный вами класс, создав несколько экземпляров класса (в методе main()), описывающего предмет,
 * провести инициализацию переменных-атрибутов каждого экземпляра и вызвать его методы.
 * 5. Имя пакета в котором создается класс/классы должно иметь формат ua.org.oa.<Jira Login>.
 *
 * Объект:
 * Запись в блоге
 *
 * Created by Alex on 20.09.2016.
 */
public class BlogPost {
    public boolean published; // Публикация поста
    public String postTitle; // Заголовок поста
    public String authorName; // Автор поста
    public int symbols; // Количество симсолов поста
    public int comments; // Количество комментариев поста


    // Конструктор  класса BlogPost


    public BlogPost(boolean published, String postTitle, String authorName, int symbols, int comments) {
        this.published = published;
        this.postTitle = postTitle;
        this.authorName = authorName;
        this.symbols = symbols;
        this.comments = comments;
    }

    // Проверить публикацию поста
    public void publish() {
        if (published == true) {
            if (symbols < 20 & comments < 5) {
                System.out.println("'" + postTitle + "'" + " " + "'" + authorName + "'" + " - Нафиг ты это говно опубликовал?!");
            } else if (symbols > 20 & comments > 5) {
                System.out.println("'" + postTitle + "'" + " " + "'" + authorName + "'" + " - Огонь! Пиши ещё!");
            }
        }else {
            System.out.println("'Статья не опубликована'");
        }
    }
}
