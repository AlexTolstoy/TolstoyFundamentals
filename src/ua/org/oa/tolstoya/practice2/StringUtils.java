package ua.org.oa.tolstoya.practice2;

import java.sql.Struct;

/**
 * Created by Alex on 22.09.2016.
 */
public class StringUtils {
    public static String stringConcat (String a, String s) {
        a = a.substring(1);
        s = s.substring(1);
        return a.concat(s);
    }

    public static String stringSub (String d) {
        String gh = null;
        if (d.length()%2 != 0 && d.length() >= 3) {
            int fg = d.length() / 2;
            gh = d.substring((fg - 1), (fg + 2));
        } else {
            System.out.println("Строка с четным количеством символов");
        }
        return gh;
    }

    public static String stringAdd (String f) {
        return f.substring(f.length()-2).concat(f.substring(0, f.length() - 2));
    }

    public static void lettersClone (String j) {
        StringBuilder sb = new StringBuilder(j);
        for (int i = 0; i < sb.length(); i += 2) {
            sb.insert(i + 1, sb.charAt(i));
        }
        System.out.println(sb);
    }

    public static void repeat(String s) {
        int count = 0;
        for (int i = 0; i <s.length() - 1 ; i++) {
            if (s.charAt(i) == 'b' && s.charAt(i + 2) == 'b') {
                count++;
            }
        }
        System.out.println(count);
    }

    public static void charDelete(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length() - 1 ; i++) {
            if (sb.charAt(i) == '*') {
                sb.delete(i - 1, i + 2);
            }
        }
        System.out.println(sb);
    }

    public static void amountWord(String s) {
        int count = 0;
        for (int i = 0; i < s.length() - 1; i++) {
            if ((s.charAt(i) == 'a' || s.charAt(i) == 's') && (s.charAt(i + 1) == ' ')) {
                count++;
            }
        }
        if (s.charAt(s.length() - 1) == 'a' || s.charAt(s.length() - 1) == 's') {
            count++;
        }
        System.out.println(count);
    }

    public static void newString(String s, String z) {
        System.out.println(s.replace(z, ""));
    }
}
