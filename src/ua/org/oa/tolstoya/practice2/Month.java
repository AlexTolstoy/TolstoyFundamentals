package ua.org.oa.tolstoya.practice2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 05.11.2016.
 */
public class Month {
    private String month;
    static final String numbers = "-0123456789";
    private int day;
    List<Integer> temperature;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public String readConsole(String message) {
        try {
            System.out.println(message);
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void inputMonth() {
        String input = null;
        boolean isNotCorect = true;
        while (isNotCorect) {
            input = readConsole("Введите мясяц");
            isNotCorect = false;
            switch (input) {
                case "Январь":
                    day = 3;
                    break;
                case "Февраль":
                    day = 2;
                    break;
                case "Март":
                    day = 5;
                    break;
                default:
                    System.out.println("Некорректный месяц - " + input);
                    isNotCorect = true;
                    break;
            }
        }
        month = input;
    }

    public void inputTemp() {
        temperature = new ArrayList<>(day);
        String input = null;
        for (int i = 0; i < day;) {
            input = readConsole("ВВедиье температуру за " + (i + 1) + " число");
            if(validInt(input)) {
                int temp = Integer.parseInt(input);
                if(temp < 51 && temp > -51) {
                    temperature.add(temp);
                    i++;
                } else {
                    System.out.println("Неправильный диапозон температур");
                }
            } else {
                System.out.println("Некорректный ввод " + input);
            }
        }
    }

    public boolean validInt(String s) {
        for (int i = 0; i < s.length(); i++) {
            if(numbers.indexOf(s.charAt(i)) == -1){
                return false;
            }
        }
        return true;
    }

    public void averageTemp() {
        double sumTemp = 0;
        for (Integer integer : temperature) {
            sumTemp += integer;
        }
        System.out.println("Средняя температура " + sumTemp/day);
    }

    public void minMax() {
        int min = temperature.get(0);
        int minDay = 1;
        int max = temperature.get(0);
        int maxDay = 1;
        for (int i = 1; i < day; i++) {
            if(min > temperature.get(i)) {
                min = temperature.get(i);
                minDay = i + 1;
            }
            if(max < temperature.get(i)) {
                max = temperature.get(i);
                maxDay = i + 1;
            }

        }
        System.out.println("Минимальная температура " + min + " за " + minDay + " число");
        System.out.println("Максимальная температура " + max + " за " + maxDay + " число");
    }
}
