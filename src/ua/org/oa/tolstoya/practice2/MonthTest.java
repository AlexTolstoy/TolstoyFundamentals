package ua.org.oa.tolstoya.practice2;

/**
 * Created by Alex on 05.11.2016.
 */
public class MonthTest {
    public static void main(String[] args) {
        Month month = new Month();
        month.inputMonth();
        month.inputTemp();
        month.averageTemp();
        month.minMax();
    }
}
